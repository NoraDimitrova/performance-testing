# Performance Testing

Performance Testing - HomeworkWorkshop

Using the setup presented in the lecture, complete the following Thread Group Scenarios:

**Scenario 1:

- Get all posts
- Create a post
- Update a post
- Get all posts

**Scenario 2:

- Get all posts
- Create a post
- Create a comment in the post
- Delete post
- Run Configurations:

Configure run for Performance Test with a duration of 1 minute, and load  10 users
Configure run for Spike test with duration 5 minutes and maximum load of 100 users
Configure run for Load test with a maximum load of 60 users

**Assertions:

 - Assert that the response contains specific text
 - Assert response Status code
 - Assert response data by JSON Path 